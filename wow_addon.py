#!/usr/bin/python3

import os
import shutil
import threading
import requests
import time
from lib import config, addon
from pathlib import Path

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def remove_files(directory):
    if os.path.exists(directory):
        files = os.listdir(directory)
        for file in files:
            file = os.path.join(directory, file)
            if os.path.isfile(file):
                os.remove(file)
            if os.path.isdir(file):
                remove_files(file)
        os.rmdir(directory)


def remove_folders(addonfolder, dirs_arr):
    for directory in dirs_arr:
        combined_path = Path(addonfolder + "/" + directory)
        remove_files(combined_path)


def remove_config_folders(conf, section_header, addonfolder):
    dirs = conf.get_value(section_header, "addon_dirs")
    if dirs is not None:
        dirs_arr = dirs.split(",")
        for directory in dirs_arr:
            combined_path = Path(addonfolder + "/" + directory)
            remove_files(combined_path)


def process_addon(addon_line, addon_sema, settings, conf, return_vals):
    try:
        new_addon = addon.Addon(addon_line)
        if new_addon.site is None:
            print("[~] " + "'"+addon_line+"' is not a compatible addon link. Skipping.")
            return_vals.append(1)
            return

        count = 0
        # Sometimes Curse website can return an invalid page, just needs to refresh
        while new_addon.site.directLink is None:
            if count > settings.retry_download_count:
                break

            new_addon.site.parse_download_page()
            count += 1

        installed_version = conf.get_value(new_addon.addonUrl, "version")

        # Check that the version on the page could be found.
        if new_addon.site.remoteVersion.isspace():
            print("[?] " + "Couldn't find remote version number for '" + new_addon.addonName + "'.")
            return_vals.append(1)
            return

        # Check that the addon is installed, and that the addon is up to date
        if (installed_version is not None) and (new_addon.site.remoteVersion == installed_version):
            print("[.] " + new_addon.site.remoteVersion + " | " + new_addon.addonName + " is up to date.")
            return_vals.append(0)
            return

        # Check if the download link could be found on the page
        if new_addon.site.directLink.isspace():
            print("[?] " + "Couldn't find the download link on page for '" + new_addon.addonName + "'.")
            return_vals.append(1)
            return

        download_file = new_addon.site.download_addon(new_addon.site.directLink, tempPath, settings.retry_download_count)
        if download_file is None:
            print("[!] " + "Failed to download '" + new_addon.addonName + "' after " + str(settings.retry_download_count) + " times.")
            return_vals.append(1)
            return
            
        new_addon.site.extract_and_remove_file(download_file, tempPath)

        remove_folders(settings.addon_destination, new_addon.addonDirectories)
        #time.sleep(1)
        new_addon.site.move_folders(tempPath, settings.addon_destination)

        print("[*] " + str(installed_version) + " -> " + new_addon.site.remoteVersion + " | '" + new_addon.addonName + "' downloaded.")

        conf.set_section(new_addon.addonUrl, "addonname", new_addon.addonName)
        conf.set_section(new_addon.addonUrl, "version", new_addon.site.remoteVersion)
        conf.set_section(new_addon.addonUrl, "addon_dirs", ",".join(new_addon.addonDirectories))
        return_vals.append(0)
    except requests.exceptions.ConnectionError:
        print("Connection reset when downloading: " + addon_line)
        return_vals.append(1)
    finally:
        addon_sema.release()


if __name__ == "__main__":
    config_file_path = "config.txt"
    installed_file_path = "installed.txt"
    addon_file_path = "addons.txt"

    installedSettings = config.Config(installed_file_path)
    scriptSettings = config.Config(config_file_path)

    scriptSettings.addon_destination = None
    scriptSettings.retry_download_count = 4
    scriptSettings.max_thread_count = 5

    if scriptSettings.read_from_file():
        try:
            # Get destination of addons
            configAddonFilePath=scriptSettings.get_value("config", "addon_folder")
            if os.path.isdir(configAddonFilePath):
                scriptSettings.addon_destination=configAddonFilePath
                print("Using destination directory '" + scriptSettings.addon_destination + "'.")
            else:
                print("Filepath '"+configAddonFilePath+"' is not a valid directory or does not exist.")
                exit()

            retryDownloadCountConf = scriptSettings.get_value("config","retry_download_count")
            try:
                scriptSettings.retry_download_count=int(retryDownloadCountConf)
            except ValueError:
                print("'"+retryDownloadCountConf+"' is not valid for 'retry_download_count' config option.")
                exit()

            maxThreadsConf = scriptSettings.get_value("config","max_threads")
            try:
                scriptSettings.max_thread_count=int(maxThreadsConf)
                print("Using "+maxThreadsConf+" download thread(s).")
            except:
                print("'"+maxThreadsConf+"' is not valid for 'max_threads' config option.")
                exit()

        except Exception:
            print("Error reading config file. Try recreating it.")
            exit()
    else:
        print("Couldn't read settings from '" + config_file_path + "'.")
        exit()

    # Start reading from the addons.txt
    if os.path.isfile(addon_file_path):
        print("Processing addons from: " + addon_file_path)

        # Load the list of addons from textfile
        with open(addon_file_path) as f:
            addonList = [line.rstrip().strip("/") for line in f]

        # Create temp folder to download/extract addons to
        tempPath = Path(scriptSettings.addon_destination + "/temp_folder")
        if not os.path.isdir(tempPath):
            os.mkdir(tempPath)

        addon_threads = threading.Semaphore(value=scriptSettings.max_thread_count)
        threads = []
        return_vals = []

        installedSettings.read_from_file()
        for line in addonList:
            addon_threads.acquire()
            newThread = threading.Thread(target=process_addon, args=[line, addon_threads, scriptSettings, installedSettings, return_vals])
            threads.append(newThread)
            newThread.start()

        for x in threads:
            x.join()

        installedSettings.write_to_file()
        print("Successfully updated "+str(return_vals.count(0))+" addon(s). "+str(return_vals.count(1))+" error(s).")

        for section in installedSettings.get_sections():
            if section not in addonList:
                print("Uninistalling: "+section)
                remove_config_folders(installedSettings, section, scriptSettings.addon_destination)
                installedSettings.remove_section(section)

        installedSettings.write_to_file()
        print("All done!")
        input('Press enter to continue: ')
    else:
        print("Filepath '" + addon_file_path + "' does not exist. Create the file in the same directory as the script.")
