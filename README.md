# wow-addon-updater
A simple command line tool to download and update WoW curseforge addons.

![Demo](https://i.imgur.com/D8HSVsu.gifv)

## Features
* Curseforge, wowace and elvui links supported
* Uninstallation of addon files
* Batch updating
* Multi-threading

## Requirements
* [Python3](https://www.python.org/downloads/)
* [requests](https://github.com/requests/requests) library
* [lxml](https://github.com/lxml/lxml) library

Install the above requirements using;
````
pip install -r requirements.txt
````

## First time use
* Modify the **config.txt** file and change the **addon_folder=** option to the addon destination, for example;
````
addon_folder=D:\Program Files (x86)\World of Warcraft\Interface\AddOns
````

* Populate the **addons.txt** file with a list of curseforge addon links, such as;

```
https://www.curseforge.com/wow/addons/pathfinder
https://www.curseforge.com/wow/addons/apbutton
https://www.wowace.com/projects/adibags
https://wow.curseforge.com/projects/details
https://www.curseforge.com/wow/addons/dominos
https://www.tukui.org/download.php?ui=tukui
```
	
* Run the **wow_addon.py** script using;
````
python .\wow_addon.py
````
The script will then download the addons to the directory specified.

To uninstall an addon, remove the link from the **addons.txt** file and re-run the script.
