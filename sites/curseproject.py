from sites import addon_site
import lxml.html
import requests


class CurseProject(addon_site.AddonSite):

    def parse_download_page(self):
        page = requests.get(self.addon.addonUrl+"/files")
        if page:
            parse_html = lxml.html.fromstring(page.content)
            if len(parse_html):
                download_link = parse_html.xpath("//a[@class='overflow-tip twitch-link']")
                if len(download_link):
                    href = download_link[0].get("href")
                    if href:
                        self.directLink = "https://wow.curseforge.com" + str(href) + "/download"

                    ver=download_link[0].get("data-name")
                    if(ver):
                        self.remoteVersion = str(ver)