from sites import addon_site
import lxml.html
import requests


class CurseAddon(addon_site.AddonSite):

    def parse_download_page(self):
        page = requests.get(self.addon.addonUrl+"/files")
        if page:
            parse_html = lxml.html.fromstring(page.content)
            if len(parse_html):

                download_link = parse_html.xpath("//a[@class='button button--download download-button mg-r-05']")
                version_link = parse_html.xpath("//td[@class='project-file__name']")

                if len(download_link):
                    href = download_link[0].get("href")
                    if href:
                        self.directLink = "https://www.curseforge.com"+str(href)+"/file"

                if len(version_link):
                    self.remoteVersion = version_link[0].get("title")