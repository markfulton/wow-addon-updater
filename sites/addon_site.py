import requests
import zipfile
import io
import shutil
import os
from pathlib import Path


class AddonSite:

    def __init__(self, addon):
        self.addon = addon
        self.siteType = None

        self.directLink = None
        self.remoteVersion = None

    def parse_download_page(self):
        return

    def download_file(self, url, folder):
        filename = self.addon.addonName

        combined_path = Path(str(folder)+"/"+filename+".zip")
        r = requests.get(url, stream=True)
        with open(combined_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
        return combined_path

    def extract_and_remove_file(self, zipFile, dest):
        with zipfile.ZipFile(zipFile,"r") as z:
            dirs = []
            for name in z.namelist():
                split_name=str(name).split("/")[0]
                if split_name not in dirs:
                    dirs.append(split_name)

            self.addon.addonDirectories = dirs
            z.extractall(dest)
        os.remove(zipFile)

    def move_folders(self, source, dest):
        for directory in self.addon.addonDirectories:
            combined_path_source = os.path.join(source, directory)
            combined_path_dest = os.path.join(dest, directory)

            shutil.copytree(combined_path_source, combined_path_dest)
            shutil.rmtree(combined_path_source)

    def download_addon(self, url, folder, retry_times):

        count = 0
        while count < retry_times:
            try:
                if not url.isspace():
                    local_addon = self.download_file(url, folder)
                    if os.path.exists(local_addon):
                        return local_addon
            except Exception:
                pass

            count += 1

        return None
