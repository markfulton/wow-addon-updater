from sites import addon_site
import lxml.html
import requests


class ElvUI(addon_site.AddonSite):

    def parse_download_page(self):

        page = requests.get(self.addon.addonUrl)
        if page:
            parse_html = lxml.html.fromstring(page.content)
            if len(parse_html):
                download_link = parse_html.xpath("//a[@class='btn btn-mod btn-border-w btn-round btn-large']")
                if len(download_link):
                    href = download_link[0].get("href")
                    if href:
                        self.directLink = "https://www.tukui.org" + str(href)

                        href_split = str(href).split("/")
                        href_split_len = len(href_split)
                        if href_split_len > 1:
                            self.remoteVersion=str(href_split[href_split_len-1])